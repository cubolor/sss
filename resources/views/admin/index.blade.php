@extends ('layout')

@section('content')


    @foreach ($categories as $category)
        <!--begin::Portlet-->
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Vote Results for : <b>{{$category->name}}</b>
                        </h3>
                    </div>
                </div>
            </div>


            <div class="m-portlet__body">
                <!--begin::Section-->
                <div class="m-section">
                    <div class="m-section__content">

                            <table class="table table-striped m-table">
                                <thead>
                                <tr>
                                    <th>
                                        Name
                                    </th>
                                    <th>
                                        Total Votes
                                    </th>

                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($category->candidates as $candidate)
                                <tr>

                                    <td>
                                        <b>{{$candidate->name}}</b>
                                    </td>

                                    <td>
                                        <b>{{count($candidate->votes)}}</b>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                    </div>
                </div>
                <!--end::Section-->
            </div>
            <!--end::Form-->

        </div>
        <!--end::Portlet-->
    @endforeach

@endsection