@extends ('layout')

@section('content')

    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
											<span class="m-portlet__head-icon m--hide">
												<i class="la la-gear"></i>
											</span>
                    <h3 class="m-portlet__head-text">
                        Create New Category
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        <form action="/admin/category/store" class="m-form m-form--fit m-form--label-align-right" method="post">
            {{csrf_field()}}
            <div class="m-portlet__body">
                <div class="form-group m-form__group">
                    <label for="name">
                        Name
                    </label>
                    <input type="text" class="form-control m-input m-input--square" id="name" name="name" placeholder="Enter Category Name">
                    <span class="m-form__help">
                        Enter the name of category you want to create.
                    </span>
                </div>
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">
                <div class="m-form__actions">
                    <button type="submit" class="btn btn-metal">
                        Submit
                    </button>
                    <button type="reset" class="btn btn-secondary">
                        Cancel
                    </button>
                </div>
            </div>
        </form>
        <!--end::Form-->
    </div>

@endsection