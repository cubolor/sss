@extends ('layout')

@section('content')

    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Update Categories
                    </h3>
                </div>
            </div>

        </div>
        <div class="m-portlet__body">

            <table class="table table-striped">
                <thead>
                <tr>

                    <th scope="col">Index</th>
                    <th scope="col">Name</th>
                    <th scope="col">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($categories as $category)
                <tr>
                    <td><b>{{$category->id}}</b></td>
                    <td><b>{{$category->name}}</b></td>
                    <td>
                        <a href="{{route('admin.category.edit',['id'=>$category->id]) }}" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="View ">
                            <i class="la la-edit"></i>
                        </a>
                        <a href="{{route('admin.category.delete',['id'=>$category->id]) }}" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete">
                            <i class="la la-trash"></i>
                        </a>
                    </td>

                </tr>
                @endforeach
                </tbody>
            </table>

        </div>
    </div>

@endsection