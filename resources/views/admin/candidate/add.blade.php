@extends ('layout')

@section('content')

            <div class="m-portlet m-portlet--tab">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                                        <span class="m-portlet__head-icon m--hide">
                                            <i class="la la-gear"></i>
                                        </span>
                            <h3 class="m-portlet__head-text">
                                Add Candidate(s)
                            </h3>
                        </div>
                    </div>
                </div>
                <!--begin::Form-->
                <form class="m-form m-form--fit m-form--label-align-right" enctype="multipart/form-data"
                      method="post" action="{{action('CandidateController@store')}}">
                    {{csrf_field()}}
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group ">

                                <label for="example-text-input" class="col-form-label">
                                    Name:
                                </label>
                                <div class="col-13">
                                    <input class="form-control m-input" type="text" placeholder="Candidate Name"
                                           name="name"
                                           id="example-text-input">
                                </div>

                        </div>
                        <div class="form-group1 m-form__group">
                            <label for="exampleInputEmail1">
                                Category:
                            </label>
                            <div></div>
                            <select class="custom-select form-control" name="category">
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">
                                Image:
                            </label>
                            <div></div>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="customFile" name="image">
                                <label class="custom-file-label" for="customFile">
                                    Choose file
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions">
                            <button type="submit" class="btn btn-primary">
                                Submit
                            </button>
                            <button type="reset" class="btn btn-secondary">
                                Cancel
                            </button>
                        </div>
                    </div>
                </form>
                <!--end::Form-->
            </div>

@endsection