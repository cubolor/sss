@extends ('layout')

@section('content')

    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Candidates
                    </h3>
                </div>
            </div>

        </div>
        <div class="m-portlet__body">

            <table class="table table-striped">
                <thead>
                <tr>

                    <th scope="col">Name</th>
                    <th scope="col">Category</th>
                    <th scope="col">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($candidates as $candidate)
                    <tr>
                        <td><b>{{$candidate->name}}</b></td>
                        <td><b>{{$candidate->category->name}}</b></td>
                        <td>
                            <a href="{{route('admin.candidate.edit',['id'=>$candidate->id]) }}" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="View " >
                                <i class="la la-edit"></i>
                            </a>
                            <a href="{{route('admin.candidate.delete',['id'=>$candidate->id]) }}" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete">
                                <i class="la la-trash"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
    </div>

@endsection