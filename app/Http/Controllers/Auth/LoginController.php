<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Voter;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }



    public function student()
    {

        $credentials= request()->except('_token');
        $voter = Voter::where($credentials)->first();

        if(!empty($voter)){
            if (Auth::guard('voter')->loginUsingId($voter->id)) {
                return redirect()->to('/');
            }
        }



            return redirect()->back()->withErrors("Sorry Couldn't authenticate");


    }
}
