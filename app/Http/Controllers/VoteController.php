<?php

namespace App\Http\Controllers;

use App\Candidate;
use App\Category;
use App\Vote;
use Carbon\Carbon;
use Illuminate\Http\Request;

class VoteController extends Controller
{

    public function confirm(Category $category, Candidate $candidate){

        $form = request()->except('_token');


        $data =[];

        foreach ($form as $cat => $can){
            array_push($data, ['category'=>$category->find($cat)->name, 'candidate'=>$candidate->find($can)->name]);
        }

        return response()->json($data);
    }

    public function votenow(Vote $vote)
    {
        $votes =[];
        $user = auth()->id();
        $form = request()->except('_token');

        foreach ($form as $cat => $can){
            array_push($votes,['category_id'=>$cat,'candidate_id'=>$can,'voter_id'=>$user,'created_at'=>Carbon::now()]);
        }
        $voted = $vote->insert($votes);
        if($voted){
            return redirect()->back()->with('status','Vote Casted Successfully');
        }
        return redirect()->back()->withErrors('Sorry! Unable to cast vote');
    }
}
