<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    protected $categories;
    public function __construct(Category $categories)
    {
        $this->categories = $categories;
    }

    public function index()
    {
        $categories = $this->categories->with('candidates')->get();

        $votes = auth()->user()->votes;



        if(count($votes)){
            return view ('test'); //TODO:: you need to do a page for this
        }


        return view('index',compact('categories'));

    }

    public function revote(){
        $votes = auth()->user()->votes;

        foreach ($votes as $vote){

            $vote->delete();

        }

        return redirect()->back();
    }

    public function add()
    {
        return view('/admin/category/add');
    }

    public function update()
    {
        $categories = $this->categories->all();
        return view('admin/category/update', compact('categories'));
    }

    public function store(Request $request)
    {

        $name = $request->has('name')?$request->get('name'):'';

        $categories = $this->categories->create([
            'name'=>$name,
        ]);

        return redirect('/admin/category/add');
    }

    public function delete($id)
    {
        $category = Category::find($id);
        $category->delete();
        return redirect()->back();
    }

    public function edit($id)
    {
        $category = Category::find($id);

        return view('/admin/category/edit')->with('category',$category);
    }

    public function save(Request $request, $id)
    {
        $category = Category::find($id);

        $category->name = $request->name;

        $category->save();

        return redirect()->back();

    }
}
