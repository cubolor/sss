<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use App\Candidate;

class CandidateController extends Controller
{
    protected $candidates,$categories;
    public function __construct(candidate $candidates, Category $category)
    {
        $this->candidates = $candidates;
        $this->categories = $category;
    }

    public function add()
    {
        $categories = $this->categories->all();
        return view('/admin/candidate/add',compact('categories'));
    }

    public function store()
    {
        $form = request();
        $candidate = new $this->candidates();
        $candidate->name = $form->name;
        $candidate->category_id = $form->category;
        $upload = $this->upload('image');
        $upload ? $candidate->image = $upload : null;
        if($candidate->save()){
            return redirect()->back()->with('status','Candidate has been saved');
        }

        return redirect()->back()->withErrors('Error Saving Candidates');
    }

    public function update()
    {
        $candidates = $this->candidates->all();
        return view('admin/candidate/update', compact('candidates'));
    }


    private function upload($name='picture')
    {
        $fileName = null;
        $destinationPath = null;
        if (request()->hasFile($name)) {
            $file = request()->file($name);
            $destinationPath = ("uploads/$name/");
            $fileName = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
            $file->move($destinationPath, $fileName);
            return $destinationPath.$fileName;
        }

        return null;

    }
}
