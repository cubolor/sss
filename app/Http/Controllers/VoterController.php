<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Voter;

class VoterController extends Controller
{
    protected $voters;
    public function __construct(Voter $voters)
    {
        $this->voters = $voters;
    }

    public function add()
    {
        return view('/admin/voter/add');
    }

    public function update()
    {
        $voters = $this->voters->all();
        return view('/admin/voter/update', compact('voters'));
    }

    public function store(Request $request)
    {

        $index_num = $request->has('index_num')?$request->get('index_num'):'';
        $name = $request->has('name')?$request->get('name'):'';

        $voters = $this->voters->create([
            'name'=>$name,
            'index_num'=>$index_num
        ]);

        return redirect('/admin/voter/add');
    }
    public function delete($id)
    {
        $voter = Voter::find($id);
        $voter->delete();
        return redirect()->back();
    }

    public function edit($id)
    {
        $voter = Voter::find($id);

        return view('/admin/voter/edit')->with('voter',$voter);
    }

    public function save(Request $request, $id)
    {
        $voter = Voter::find($id);

        $voter->index_num = $request->index_num;
        $voter->name = $request->name;

        $voter->save();

        return redirect()->back();

    }
}
