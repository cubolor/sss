<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    protected $guarded = ['id'];

    public function voter()
    {
        return $this->belongsTo('App\Voter');
    }
}
