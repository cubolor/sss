<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Voter extends Authenticatable
{
    public $timestamps = false;

    protected $fillable = [
        'name','index_num',
    ];

    public function votes()
    {
        return $this->hasMany('App\Vote');
    }

    public function getAuthPassword()
    {
        return $this->index_num;
    }
}
