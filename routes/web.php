<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/






Route::group(['middleware'=>'auth:voter'],function(){
    Route::get('/', "CategoryController@index");
    Route::get('revote','CategoryController@revote');

    Route::get('vote/confirm','VoteController@confirm');
    Route::post('vote/now','VoteController@votenow');
});






Route::get('/test', function () {
    return view('test');
});


Route::group(['middleware'=>'auth'],function(){

    Route::get('/admin','DashboardController@index');

    Route::get('/admin/candidate/add','CandidateController@add');
    Route::get('/admin/candidate/update','CandidateController@update');
    Route::post('/admin/candidate/store','CandidateController@store');

    Route::get('/admin/candidate/delete/{id}',[
        'uses'=>'CandidateController@delete',
        'as'=>'admin.candidate.delete'
        ]);



    Route::get('/admin/category/add','CategoryController@add');
    Route::get('/admin/category/update','CategoryController@update');
    Route::post('/admin/category/store','CategoryController@store');

    Route::get('/admin/category/delete/{id}',[
        'uses'=>'CategoryController@delete',
        'as'=>'admin.category.delete'
    ]);

    Route::get('/admin/category/edit/{id}',[
        'uses'=>'CategoryController@edit',
        'as'=>'admin.category.edit'
    ]);

    Route::post('/admin/category/save/{id}',[
        'uses'=>'CategoryController@save',
        'as'=>'admin.category.save'
    ]);



    Route::get('/admin/voter/add','VoterController@add');
    Route::get('/admin/voter/update','VoterController@update');
    Route::post('/admin/voter/store','VoterController@store');

    Route::get('/admin/voter/delete/{id}',[
        'uses'=>'VoterController@delete',
        'as'=>'admin.voter.delete'
    ]);

    Route::get('/admin/voter/edit/{id}',[
        'uses'=>'VoterController@edit',
        'as'=>'admin.voter.edit'
    ]);

    Route::post('/admin/voter/save/{id}',[
        'uses'=>'VoterController@save',
        'as'=>'admin.voter.save'
    ]);

    Route::get('/home', 'HomeController@index')->name('home');


});


    Route::post('/student/login', 'Auth\LoginController@student');



    Auth::routes();

    Route::get('logout', 'Auth\LoginController@logout')->name('logout');

    Route::get('/admin/login','AdminController@index')->name('/admin/auth/admin');



